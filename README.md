# Information / Информация

Блок со скрытой и отображаемой информацией.

## Install / Установка

1. Загрузите папки и файлы в директорию `extensions/MW_EXT_Spoiler`.
2. В самый низ файла `LocalSettings.php` добавьте строку:

```php
wfLoadExtension( 'MW_EXT_Spoiler' );
```

## Syntax / Синтаксис

```html
<spoiler title="[TITLE]">[CONTENT]</spoiler>
```

## Donations / Пожертвования

- [Donation Form](https://donation-form.github.io/)
